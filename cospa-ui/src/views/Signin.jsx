import {
  Box,
  Button,
  Container,
  Link,
  TextField,
  Typography,
} from '@mui/material/'

import { Link as RouterLink } from 'react-router-dom'
import useCospaStore from '../stores/cospaStore'

const Signin = () => {
  const { signin, error } = useCospaStore((state) => state)

  const handleSubmit = (event) => {
    event.preventDefault()
    const data = new FormData(event.currentTarget)
    signin(data.get('email'), data.get('password'))
  }

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" variant="h5">
          Einloggen bei CoSPA
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email-Adresse"
            name="email"
            autoComplete="email"
            autoFocus
            defaultValue="testUser123"
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            defaultValue="123456"
          />

          {error && (
            <Typography
              variant="body2"
              sx={{ color: 'red', textAlign: 'center' }}
            >
              {error.message}
            </Typography>
          )}

          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            color="warning"
          >
            Anmelden
          </Button>

          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Link component={RouterLink} to="/signup" variant="body2">
              Zur Registrierung
            </Link>
          </Box>
        </Box>
      </Box>
    </Container>
  )
}

export default Signin
