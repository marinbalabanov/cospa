import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Link as RouterLink } from 'react-router-dom'
import { Box, Button, Container, Typography } from '@mui/material'
import useCospaStore from '../stores/cospaStore'

const SpaceDetails = () => {
  const [id, setId] = useState('')
  const { logout, getSpace, space } = useCospaStore((state) => state)

  const getParams = useParams()
  useEffect(() => {
    const { id } = getParams
    setId(id)
  }, [getParams])

  useEffect(() => {
    if (!id) {
      return
    }
    getSpace(id)
  }, [id])

  const handleLogout = () => {
    logout()
  }

  return (
    <Container
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography variant="h1">Space Details</Typography>
      <Typography variant="h2">Details von Space mit der ID </Typography>
      <Box sx={{ mb: 2, border: '1px solid black', p: 4 }}>
        <pre>{JSON.stringify(space, null, 2)}</pre>
      </Box>

      {/* Add a link to welcome */}
      <Button
        component={RouterLink}
        to="/"
        variant="contained"
        color="primary"
        sx={{ mb: 2 }}
      >
        Zurück zur Startseite
      </Button>

      <Button
        variant="contained"
        color="error"
        sx={{ mb: 2 }}
        onClick={handleLogout}
      >
        Ausloggen
      </Button>
    </Container>
  )
}

export default SpaceDetails
