import { useState, useEffect } from 'react'
import {
  Button,
  TextField,
  Grid,
  Box,
  Typography,
  Link,
  Alert,
} from '@mui/material'
import { Link as RouterLink, useNavigate } from 'react-router-dom'

import useCospaStore from '../stores/cospaStore'
import useForm from '../hooks/useForm'

const Signup = () => {
  const { signup, newUser, error } = useCospaStore((state) => state)
  const [warning, setWarning] = useState()

  const { formState, handleChange } = useForm({
    email: 'test@lorem.ipsum',
    firstName: 'John',
    lastName: 'Doe',
    password: '',
    passwordConfirm: '',
  })

  const navigate = useNavigate()

  useEffect(() => {
    if (newUser) {
      navigate('/login')
    }
  }, [newUser])

  const handleSignup = () => {
    if (formState.password !== formState.passwordConfirm) {
      setTimeout(() => {
        setWarning(null)
      }, 5000)
      return setWarning('Die beiden Kennwörter stimmen nicht überein')
    }

    signup(
      formState.email,
      formState.firstName,
      formState.lastName,
      formState.password
    )
  }

  return (
    <>
      <Box
        sx={{
          marginTop: 4,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography component="h1" variant="h5">
          Signup bei CoSPA
        </Typography>

        {newUser && (
          <Alert severity="success" sx={{ minWidth: '100%' }}>
            {`Ihr User wurde erfolgreich angelegt!`}
          </Alert>
        )}

        {warning && (
          <Alert severity="warning" sx={{ minWidth: '60%', margin: '0 auto' }}>
            {warning}
          </Alert>
        )}

        <Grid container spacing={2} sx={{ mt: 4, width: '60%' }}>
          <Grid item xs={4}>
            <TextField
              fullWidth
              required
              label="Email-Adresse"
              name="email"
              value={formState.email}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              fullWidth
              required
              type="password"
              label="Kennwort"
              name="password"
              value={formState.password}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              fullWidth
              required
              type="password"
              label="Kennwort-Bestätigung"
              name="passwordConfirm"
              value={formState.passwordConfirm}
              onChange={handleChange}
            />
          </Grid>

          <Grid item xs={6}>
            <TextField
              fullWidth
              required
              label="Vorname"
              name="firstName"
              value={formState.firstName}
              onChange={handleChange}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              required
              label="Nachname"
              name="lastName"
              value={formState.lastName}
              onChange={handleChange}
            />
          </Grid>
        </Grid>

        {error && (
          <Typography variant="body2" sx={{ color: 'red' }}>
            {error.message}
          </Typography>
        )}

        <Button
          onClick={handleSignup}
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
          color="warning"
        >
          Jetzt registrieren
        </Button>

        <Link component={RouterLink} to="/login" variant="body2">
          Hier einloggen
        </Link>
      </Box>
    </>
  )
}

export default Signup
