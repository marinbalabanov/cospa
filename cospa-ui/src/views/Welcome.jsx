import { useEffect } from 'react'
import { Button, Container, Typography } from '@mui/material'
import { Link as RouterLink } from 'react-router-dom'
import useCospaStore from '../stores/cospaStore'

const Welcome = () => {
  const { logout, getSpaces, spaces } = useCospaStore((state) => state)

  useEffect(() => {
    getSpaces()
  }, [])

  const handleLogout = () => {
    logout()
  }

  return (
    <Container
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Typography variant="h1">Herzlich Willkommen</Typography>
      {spaces.map((space) => (
        <>
          <pre>{JSON.stringify(space, null, 2)}</pre>
          <Button
            key={space._id}
            component={RouterLink}
            to={`/space/${space._id}`}
            variant="contained"
            color="primary"
            sx={{ mb: 2 }}
          >
            {space.name}
            <br />
            {space._id}
          </Button>
        </>
      ))}
      <Button
        variant="contained"
        color="error"
        sx={{ mb: 2 }}
        onClick={handleLogout}
      >
        Ausloggen
      </Button>
    </Container>
  )
}

export default Welcome
