const Header = () => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 4,
      }}
    >
      <img src="/cospa-logo.png" alt="CoSPA Logo" />
    </div>
  )
}

export default Header
