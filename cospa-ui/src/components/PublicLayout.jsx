import { Outlet } from 'react-router-dom'
import { Box } from '@mui/material'
import Header from './Header.jsx'

const PublicLayout = () => {
  return (
    <Box>
      <Header />
      <Outlet />
    </Box>
  )
}

export default PublicLayout
