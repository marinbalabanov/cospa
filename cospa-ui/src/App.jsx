import { useEffect } from 'react'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

import useCospaStore from './stores/cospaStore'

import { publicRoutes, privateRoutes } from './routes'

const App = () => {
  const { user, checkToken } = useCospaStore((state) => state)

  useEffect(() => {
    checkToken()
  }, [])

  const router = createBrowserRouter(user ? privateRoutes : publicRoutes)
  return <RouterProvider router={router} />
}

export default App
