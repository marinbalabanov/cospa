import axios from 'axios'

const fetchAPI = (options = {}) => {
  const defaultConfig = {
    method: 'get',
    timeout: 5000,
    data: {},
    url: '/',
  }

  const headers = options.token
    ? {
        Authorization: 'Bearer ' + options.token,
      }
    : {}

  const axiosConfig = {
    ...defaultConfig,
    ...options,
    headers,
  }

  return axios(axiosConfig)
}

export { fetchAPI }
