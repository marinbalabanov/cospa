import { create } from 'zustand'
import { jwtDecode } from 'jwt-decode'
import { fetchAPI } from '../utils'
import { HOST } from '../utils/constants'

const initialState = {
  user: null,
  loading: null,
  error: null,
  token: null,
  decodedToken: null,
  newUser: null,
  selectedUser: null,
  email: null,
  password: null,
  success: false,
  spaces: [],
}

const SECONDS_TO_RELOGIN = 30

const useCospaStore = create((set, get) => ({
  ...initialState,
  checkToken: () => {
    if (get().user) {
      if (get().decodedToken.exp - +new Date() / 1000 < SECONDS_TO_RELOGIN) {
        if (get().email && get().password) {
          return get().signin(get().email, get().password)
        }
        return get().logout()
      }
      return
    }

    const token = localStorage.getItem('token')
    if (!token) {
      return
    }

    const decodedToken = jwtDecode(token)
    set({ token, decodedToken })

    fetchAPI({ url: HOST + '/users/' + decodedToken.id })
      .then((response) => {
        set({ user: response.data })
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  signin: (email, password) => {
    set({ loading: true, error: null, user: null })

    fetchAPI({
      url: HOST + '/users/login',
      method: 'post',
      data: { email, password },
    })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Invalid status code')
        }
        const token = response.data
        const decodedToken = jwtDecode(token)
        set({ token, decodedToken, email, password })
        localStorage.setItem('token', token)
        return fetchAPI({ url: HOST + '/users/' + decodedToken.id })
      })
      .then((response) => {
        set({ user: response.data })
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  signup: (email, firstName, lastName, password) => {
    set({ loading: true, error: null, user: null })

    fetchAPI({
      url: HOST + '/users/signup',
      method: 'post',
      data: { email, firstName, lastName, password },
    })
      .then((response) => {
        if (response.status === 200) {
          set({ newUser: response.data })
        } else {
          throw new Error('Server returned invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  logout: () => {
    console.log('In logout')
    localStorage.removeItem('token')
    set({ ...initialState })
  },

  togglePaused: () => {
    const data = { paused: !get().user.paused }
    fetchAPI({
      url: HOST + '/users/' + get().user._id,
      method: 'patch',
      data,
      token: get().token,
    })
      .then((response) => {
        if (response.status === 200) {
          set({ user: response.data })
        } else {
          throw new Error('The server returned an invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  editProfile: (data) => {
    set({ loading: true, error: null, success: false })

    fetchAPI({
      url: HOST + '/users/' + get().user._id,
      method: 'patch',
      data,
      token: get().token,
    })
      .then((response) => {
        if (response.status === 200) {
          set({ user: response.data, success: true })
        } else {
          throw new Error('Vom Server kam was komisches')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  changePassword: (oldPassword, newPassword) => {
    set({ loading: true, error: null, success: false })

    fetchAPI({
      url: HOST + '/users/change-password',
      method: 'patch',
      data: { oldPassword, newPassword },
      token: get().token,
    })
      .then((response) => {
        if (response.status === 200) {
          set({ success: true })
        } else {
          throw new Error('Server returned invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },

  getOneUser: (id) => {
    console.log('In getOneUser')
    set({ loading: true, error: null, success: false, selectedUser: null })

    fetchAPI({
      url: HOST + '/users/' + id,
    })
      .then((response) => {
        if (response.status === 200) {
          set({ success: true, selectedUser: response.data })
          console.log('Response Data')
          console.log(response.data)
        } else {
          throw new Error('Server returned invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },
  getSpaces: () => {
    set({ loading: true, error: null, success: false, spaces: [] })

    fetchAPI({
      url: HOST + '/spaces/',
      token: get().token,
    })
      .then((response) => {
        if (response.status === 200) {
          set({ success: true, spaces: response.data })
        } else {
          throw new Error('Server returned invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },
  getSpace: (id) => {
    console.log(id)

    set({ loading: true, error: null, success: false, space: null })

    fetchAPI({
      url: HOST + '/spaces/' + id,
      token: get().token,
    })
      .then((response) => {
        if (response.status === 200) {
          console.log('Single space response.data')
          console.log(response.data)
          set({ success: true, space: response.data })
        } else {
          throw new Error('Server returned invalid status code')
        }
      })
      .catch((error) => {
        set({ error })
      })
      .finally(() => {
        set({ loading: false })
      })
  },
}))

export default useCospaStore
