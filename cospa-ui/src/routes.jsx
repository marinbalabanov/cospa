import { Navigate } from 'react-router-dom'

import MainLayout from './components/MainLayout'
import PublicLayout from './components/PublicLayout'

import Signin from './views/Signin'
import Signup from './views/Signup'
import Welcome from './views/Welcome'
import SpaceDetails from './views/SpaceDetails'

const publicRoutes = [
  {
    path: '',
    element: <PublicLayout />,
    children: [
      {
        path: '/',
        element: <Signin />,
      },
      {
        path: 'signin',
        element: <Signin />,
      },
      {
        path: 'signup',
        element: <Signup />,
      },
      {
        path: '*',
        element: <Navigate to="/signin" />,
      },
    ],
  },
]

const privateRoutes = [
  {
    path: '',
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: <Welcome />,
      },
      {
        path: 'space/:id',
        element: <SpaceDetails />,
      },
      {
        path: '*',
        element: <Navigate to="/" />,
      },
    ],
  },
]

export { publicRoutes, privateRoutes }
