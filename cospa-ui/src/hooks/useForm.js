import { useState } from 'react'

const useForm = (defaultFormState = {}) => {
  const [formState, setFormState] = useState(defaultFormState)

  const handleChange = (event) => {
    const { name, value } = event.target
    const newFormState = { ...formState }
    newFormState[name] = value

    setFormState(newFormState)
  }

  return { formState, handleChange, setFormState }
}

export default useForm
