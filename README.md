# CoSpa - Die Coworking Web App

Diese Web-App wird im Rahmen des Kurses "Ausbildung zum/zur geprüften Full Stack Javascript Developer:in" am WIFI Wien entwickelt. Sie soll Coworking Spaces und Coworking User zusammenbringen.

## Über die SPA

**CoSpa** ist eine Single Page Application (SPA), in der User sich registrieren können, nach Coworking Spaces in ihrer Nähe suchen und dann einen Arbeitsplatz oder einen Meetingraum anhand eines Lageplans reservieren können.

Das Frontend der SPA wird mit React und MaterialUI entwickelt und auf Render.com gehostet. Es kommuniziert mit einer REST-API. Die Daten werden in einer MongoDB-Datenbank gespeichert. Die MongoDB-Datenbank wird auf Atlas gehostet.

## Übersichtsdiagramm

![Übersichtsdiagramm](./docs/overview.png)

## Erste Beschreibung der Features

Im Folgenden werden die Features der SPA grob beschrieben. Dies dient als erstes Umsetzungskonzept und wird im Laufe der Entwicklung noch angepasst.

1. **User Management:** User-Registrierung, Login, Profil bearbeiten, PW ändern.

![Skizze des User Managements](./docs/1-user-management.png)

2. **Manuelle Auswahl des Coworking Spaces:** User können in einer Liste der Coworking Spaces nach einem geeigneten Space nach verschiedenen Kriterien suchen (z.B. Preis, Ausstattung, Öffnungszeiten, Standort etc.). Den Coworking Space auswählen und mehr Details erfahren.

![Skizze der manuellen Auswahl und Detailansicht](./docs/2-manuelle-coworking-space-auswahl.png)

3. **Location Service:** Der User-Standort wird in einer Stadt ermittelt oder manuell eingegeben, und die Coworking Spaces in der Nähe auf einer Karte angezeigt. User können dann einen Space auswählen und mehr Details erfahren. Hierfür wird die Google Maps API verwendet.

![Skizze des Location Service mit den Coworking Spaces in der Nähe](./docs/3-location-service.png)

4. **Coworking Space Detailansicht:** User erfahren die Details zu einem Coworking Space (Name, Beschreibung, Standort, Anfahrtsplan, Kontaktinfos, Preise für die verschiedenen Pakete). User können hier auch einen Lageplan des jeweiligen Coworking Space abrufen.

![Skizze der Detailansicht](./docs/4-detailansicht.png)

5. **Lageplan der Arbeitsplätze und Meetingräume in einem Coworking Space:** Im ausgewählten Coworking Space können User anhand eines Lageplans sehen, welche Arbeitsplätze und Meetingräume frei sind und an welchen Tagen und Uhrzeiten gebucht werden können. Hier können User ein Paket buchen und einen Arbeitsplatz reservieren (Art und Dauer) bzw. einen Meetingraum buchen. Das wird ein Bild sein, das ein Image-Map ist. Die Image-Map wird mit einem Tool erstellt. Die Koordinaten der einzelnen Arbeitsplätze und Meetingräume werden mit der ID verbunden.

![Skizze des Lageplans](./docs/5-lageplan.png)

6. **User-Coworking-Kalender:** User können ihre Reservierungen in einer Kalenderansicht einsehen und ggf. absagen.

![Skizze der Kalenderansicht](./docs/6-kalenderansicht.png)

## Entity Relationship Diagram (ERD)

![ERD](./docs/db-erd.png)

Jeder Coworking Space hat viele Desks (Arbeitsplätze) und einen Lagepläne. Jeder Desk hat Koordinaten auf dem Lageplan. Eine Buchung vereint einen Desk und einen User für einem bestimmten Zeitraum (Start-Datum und Uhrzeit, End-Datum und Uhrzeit).

## Außerhalb des Scopes

Im Idealfall hätte die App nicht nur die User-Rolle _Coworking-User_ sondern auch _Coworking-Betreiber_. Die Betreiber könnten sich registrieren und ihre Coworking Spaces eintragen und verwalten. Dazu gehören die Standortdaten, Beschreibungen und Paketpreise. Zudem könnten die Betreiber die Lagepläne der Standorte mit den entsprechenden Arbeitsplätzen und Meetingräumen hochladen und konfigurieren. Dazu wird die Umsetzung von Benutzerrollen sowie eine Benutzerschnittstelle für die Administration.

Dieser Use Case ist aber außerhalb des Scopes dieser App, weil die verfügbare Zeit zur Umsetzung nicht ausreicht. Für die erste Version der Web App werden Testdaten vom Entwickler im Backend eingegeben.

Zudem ist die Umsetzung eines Zahlungssystems für die Buchung der Coworking Spaces außerhalb des Scopes. Die Buchung der Coworking Spaces ist in der ersten Version der App nur ein Proof of Concept und wird nicht mit einem Zahlungssystem verknüpft.

Coworking User könnten einen Coworking Space bewerten und eine Rezension schreiben. Das wird ebenfalls in der ersten Version der App nicht umgesetzt.

## Stundenaufzeichnung

| Datum                         | Uhrzeit       | Stundenanzahl | Beschreibung                                   |
| ----------------------------- | ------------- | ------------- | ---------------------------------------------- |
| Donnerstag, 21. Dezember 2023 | 17:00 - 22:00 | 5 Stunden     | Erstes Konzept, Dokumentation und Wireframes   |
| Freitag, 22. Dezember 2024    | 16:00 - 17:00 | 1 Stunde      | Coaching mit dem Trainer                       |
| Sonntag, 24. Dezember 2023    | 17:00 - 19:00 | 2 Stunden     | Node.js/Express API                            |
| Donnerstag, 11. Jänner 2024   | 19:00 - 23:00 | 4 Stunden     | Users API, DB-Modell / ERD Arbeitsplatzbuchung |
| Freitag, 12. Jänner 2024      | 16:00 - 17:00 | 1 Stunde      | Coaching mit dem Trainer                       |
| Montag, 15. Jänner 2024       | 17:30 - 20:30 | 3 Stunden     | DB-Modell und API Spaces, Desks, Bookings      |

## Projektleistungen zum ersten Coaching am 22. Dezember 2024

1. Erstes Konzept
2. Dokumentation und Wireframes der Features
3. Einrichtung der Datenbank

## Projektleistungen zum zweiten Coaching am 12. Jänner 2024

1. NodeJS Backend mit User APIs (Registrierung, Login, Profil bearbeiten, PW ändern)
2. MongoDB und Models für User
3. Entwurf DB Modell Coworking Space

## Projektleistungen zum dritten Coaching am 19. Jänner 2024

1. Umsetzung des DB Models lt. ERD und Eingabe der erste Testdaten
2. Umsetzung der API Endpoints für die Coworking-Arbeitsplätze (lesend - Testdaten)
3. Umsetzung des API Endpoints für die Buchung eines Desks/Arbeitsplatzes in einem Coworking Space

## Projektleistungen zum vierten Coaching am 26. Jänner 2024

0. Bugs in der API beheben
1. React View: Signup und signin
2. React View: Fuer User alle Coworking Spaces auflisten
3. React View: Detailansicht Coworking Space

## Projektleistungen zum vierten Coaching am 2. Februar 2024

1. Swagger API Dokumentation
2. Styling der React Views
3. Buchen eines Platzes im Space
