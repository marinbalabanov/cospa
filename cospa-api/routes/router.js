import { Router } from 'express'
import { body } from 'express-validator'

import { checkToken } from '../common/index.js'

import {
  getAllUsers,
  getOneUser,
  signup,
  unlock,
  login,
  deleteUser,
  changePassword,
  updateUser,
} from '../controllers/users.js'

import { getAllSpaces, getOneSpace } from '../controllers/spaces.js'
import {
  getAllDesksForThisSpace,
  getOneDeskByID,
  getAllDesksForAllSpaces,
  createDesk,
  deleteDesk,
} from '../controllers/desks.js'
import {
  getAllBookingsForThisUser,
  getOneBookingByID,
  createBooking,
  getAllBookingsByDeskID,
  findBookingsByDateTimeAndDeskID,
  deleteBookingByID,
} from '../controllers/bookings.js'

const router = new Router()

/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Retrieve a list of all users
 *     description: Retrieve a list of users from the system.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: header
 *         name: Authorization
 *         required: true
 *         schema:
 *           type: string
 *         description: Bearer token for authentication.
 *     responses:
 *       '200':
 *         description: List of users retrieved successfully
 *       '401':
 *         description: Unauthorized, invalid or expired token
 *       '403':
 *         description: Forbidden, user does not have permission to access this resource
 *       '500':
 *         description: Internal server error
 */
router.get('/users', checkToken, getAllUsers)

/**
 * @swagger
 * /api/users/{userId}:
 *   get:
 *     summary: Retrieve user information by ID
 *     description: Retrieve user information by providing a valid user ID.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: string
 *         description: The unique ID of the user to retrieve.
 *       - in: header
 *         name: Authorization
 *         required: true
 *         schema:
 *           type: string
 *         description: Bearer token for authentication.
 *     responses:
 *       '200':
 *         description: User information retrieved successfully
 *       '401':
 *         description: Unauthorized, invalid or expired token
 *       '403':
 *         description: Forbidden, user does not have permission to access this resource
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 */
router.get('/users/:id', checkToken, getOneUser)

/**
 * @swagger
 * /api/users/signup:
 *   post:
 *     summary: Create a new user account
 *     description: Create a new user account with the provided email, first name, last name, and password.
 *     tags:
 *       - Users
 *     requestBody:
 *       description: JSON request body for user signup
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *               firstName:
 *                 type: string
 *               lastName:
 *                 type: string
 *               password:
 *                 type: string
 *                 minLength: 8
 *                 maxLength: 50
 *             required:
 *               - email
 *               - firstName
 *               - lastName
 *               - password
 *     responses:
 *       '201':
 *         description: User account created successfully
 *       '400':
 *         description: Bad request, validation failed or required fields missing
 *       '500':
 *         description: Internal server error
 */
router.post(
  '/users/signup',
  [
    body('email').toLowerCase().normalizeEmail().isEmail(),
    body('firstName').trim().isLength({ min: 2, max: 50 }),
    body('lastName').trim().isLength({ min: 2, max: 50 }),
    body('password').trim().isLength({ min: 6, max: 50 }),
  ],
  signup
)

/**
 * @swagger
 * /api/users/unlock:
 *   post:
 *     summary: Unlock a user's account
 *     description: Unlock a user's account using an unlock key.
 *     tags:
 *       - Users
 *     requestBody:
 *       description: JSON request body for unlocking a user's account
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               Unlock_Key:
 *                 type: string
 *             required:
 *               - Unlock_Key
 *     parameters:
 *       - in: header
 *         name: Unlock_Key
 *         schema:
 *           type: string
 *         required: true
 *         description: The unlock key to unlock the user's account.
 *     responses:
 *       '200':
 *         description: User account unlocked successfully
 *       '400':
 *         description: Bad request, validation failed or unlock key missing
 *       '401':
 *         description: Unauthorized, invalid unlock key
 *       '500':
 *         description: Internal server error
 */
router.post('/users/unlock', unlock)

/**
 * @swagger
 * /api/users/login:
 *   post:
 *     summary: User login
 *     description: Authenticate a user by providing their email and password.
 *     tags:
 *       - Users
 *     requestBody:
 *       description: JSON request body for user login
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 format: email
 *               password:
 *                 type: string
 *                 minLength: 8
 *                 maxLength: 50
 *             required:
 *               - email
 *               - password
 *     responses:
 *       '200':
 *         description: User authenticated successfully
 *       '400':
 *         description: Bad request, validation failed or required fields missing
 *       '401':
 *         description: Unauthorized, invalid email or password
 *       '500':
 *         description: Internal server error
 */
router.post('/users/login', login)

/**
 * @swagger
 * /api/users/{userId}:
 *   patch:
 *     summary: Update user information
 *     description: Update user information by providing a valid user ID and the fields to be updated.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: string
 *         description: The unique ID of the user to update.
 *       - in: header
 *         name: Authorization
 *         required: true
 *         schema:
 *           type: string
 *         description: Bearer token for authentication.
 *     requestBody:
 *       description: JSON request body for updating user information
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               firstName:
 *                 type: string
 *               lastName:
 *                 type: string
 *               paused:
 *                 type: boolean
 *             required:
 *               - firstName
 *               - lastName
 *     responses:
 *       '200':
 *         description: User information updated successfully
 *       '400':
 *         description: Bad request, validation failed or required fields missing
 *       '401':
 *         description: Unauthorized, invalid or expired token
 *       '403':
 *         description: Forbidden, user does not have permission to update this resource
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 */
router.patch(
  '/users/:id',
  checkToken,
  [
    body('firstName').trim().isLength({ min: 2, max: 50 }).optional(),
    body('lastName').trim().isLength({ min: 2, max: 50 }).optional(),
    body('paused').isBoolean().optional(),
  ],
  (req, res, next) => {
    updateUser(req, res, next)
  }
)

/**
 * @swagger
 * /api/users/change-password/{userId}:
 *   patch:
 *     summary: Change user password
 *     description: Change a user's password by providing a valid user ID, old password, and new password.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: string
 *         description: The unique ID of the user whose password needs to be changed.
 *       - in: header
 *         name: Authorization
 *         required: true
 *         schema:
 *           type: string
 *         description: Bearer token for authentication.
 *     requestBody:
 *       description: JSON request body for changing user password
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               oldPassword:
 *                 type: string
 *                 minLength: 6
 *                 maxLength: 50
 *               newPassword:
 *                 type: string
 *                 minLength: 6
 *                 maxLength: 50
 *             required:
 *               - oldPassword
 *               - newPassword
 *     responses:
 *       '200':
 *         description: User password changed successfully
 *       '400':
 *         description: Bad request, validation failed or required fields missing
 *       '401':
 *         description: Unauthorized, invalid or expired token
 *       '403':
 *         description: Forbidden, user does not have permission to change this password
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 */
router.patch(
  '/users/change-password/:id',
  checkToken,
  [
    body('oldPassword').trim().isLength({ min: 6, max: 50 }),
    body('newPassword').trim().isLength({ min: 6, max: 50 }),
  ],
  changePassword
)

/**
 * @swagger
 * /api/users/{userId}:
 *   delete:
 *     summary: Delete a user
 *     description: Delete a user by providing a valid user ID. This action is irreversible.
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         schema:
 *           type: string
 *         description: The unique ID of the user to be deleted.
 *       - in: header
 *         name: Authorization
 *         required: true
 *         schema:
 *           type: string
 *         description: Bearer token for authentication.
 *     responses:
 *       '204':
 *         description: User deleted successfully
 *       '401':
 *         description: Unauthorized, invalid or expired token
 *       '403':
 *         description: Forbidden, user does not have permission to delete this user
 *       '404':
 *         description: User not found
 *       '500':
 *         description: Internal server error
 */
router.delete('/users/:id', checkToken, deleteUser)

// Spaces

/**
 * @swagger
 * /spaces:
 *   get:
 *     tags:
 *      - Spaces
 *     summary: Get all spaces
 *     description: Get all coworking spaces, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/spaces', checkToken, getAllSpaces)

/**
 * @swagger
 * /spaces/:id:
 *   get:
 *     tags:
 *      - Spaces
 *     summary: Get one space by ID
 *     description: Get one coworking space by ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/spaces/:id', checkToken, getOneSpace)

// Desks

/**
 * @swagger
 * /desks:
 *   get:
 *     tags:
 *      - Desks
 *     summary: Get all desks
 *     description: Get all desks for all coworking spaces, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/desks', checkToken, getAllDesksForAllSpaces)

/**
 * @swagger
 * /desks/:id:
 *   get:
 *     tags:
 *      - Desks
 *     summary: Get one desk by ID
 *     description: Get one desk by ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/desks/:id', checkToken, getOneDeskByID)

/**
 * @swagger
 * /spaces/:id/desks:
 *   get:
 *     tags:
 *      - Desks
 *     summary: Get all desks for one space
 *     description: Get all desks for one coworking space, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/spaces/:id/desks', checkToken, getAllDesksForThisSpace)

/**
 * @swagger
 * /desks:
 *   post:
 *     tags:
 *      - Desks
 *     summary: Create a new desk
 *     description: Create a new desk for a coworking space, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.post('/desks', checkToken, createDesk)

/**
 * @swagger
 * /desks/:id:
 *   delete:
 *     tags:
 *      - Desks
 *     summary: Delete a desk by ID
 *     description: Delete a desk by ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.delete('/desks/:id', checkToken, deleteDesk)

// Bookings

/**
 * @swagger
 * /users/:id/bookings:
 *   get:
 *     tags:
 *      - Bookings
 *     summary: Get all bookings for a user by ID
 *     description: Get all bookings made for a specific user by the user's ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/users/:id/bookings', checkToken, getAllBookingsForThisUser)

/**
 * @swagger
 * /bookings/:id:
 *   get:
 *     tags:
 *      - Bookings
 *     summary: Get one booking by ID
 *     description: Get the details of one booking by ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/bookings/:id', checkToken, getOneBookingByID)

/**
 * @swagger
 * /desks/:id/bookings:
 *   get:
 *     tags:
 *      - Bookings
 *     summary: Get all bookings for a desk by ID
 *     description: Get all bookings made for a specific desk by the user's ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.get('/desks/:id/bookings', checkToken, getAllBookingsByDeskID)

/**
 * @swagger
 * /bookings/find:
 *   post:
 *     tags:
 *      - Bookings
 *     summary: Find a booking by date/time and desk ID
 *     description: Find a booking by providing a date, time, and desk ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.post('/bookings/find', checkToken, findBookingsByDateTimeAndDeskID)

/**
 * @swagger
 * /bookings:
 *   post:
 *     tags:
 *      - Bookings
 *     summary: Create new booking
 *     description: Create a new booking by providing a date, time, and desk ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.post('/bookings', checkToken, createBooking)

/**
 * @swagger
 * /bookings/:id:
 *   delete:
 *     tags:
 *      - Bookings
 *     summary: Delete a booking by ID
 *     description: Delete a booking by ID, the endpoint requires a valid JWT token.
 *     responses:
 *       200:
 *         description: Successful response
 */
router.delete('/bookings/:id', checkToken, deleteBookingByID)

export default router
