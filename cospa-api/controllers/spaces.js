import { Space } from '../models/spaces.js'

import HttpError from '../models/http-errors.js'

const getAllSpaces = async (req, res) => {
  const spaces = await Space.find({})
  res.json(spaces)
}

const getOneSpace = async (req, res, next) => {
  let space
  try {
    space = await Space.findById(req.params.id)
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  res.json(space)
}

const createSpace = async (req, res, next) => {
  const {
    name,
    street,
    zip,
    city,
    country,
    phone,
    email,
    website,
    planUrl,
    imageUrl,
    lat,
    lng,
  } = req.body

  const space = new Space({
    name,
    street,
    zip,
    city,
    country,
    phone,
    email,
    website,
    planUrl,
    imageUrl,
    lat,
    lng,
  })

  try {
    await space.save()
  } catch (error) {
    return next(new HttpError('Cannot create space', 500))
  }

  res.status(201).json(space)
}

const deleteSpace = async (req, res, next) => {
  let space
  try {
    space = await Space.findById(req.params.id)
  } catch (error) {
    return next(new HttpError('Cannot find space', 404))
  }

  try {
    await space.remove()
  } catch (error) {
    return next(new HttpError('Cannot delete space', 500))
  }

  res.status(204).json({ message: 'Space deleted' })
}

export { getAllSpaces, getOneSpace, createSpace, deleteSpace }
