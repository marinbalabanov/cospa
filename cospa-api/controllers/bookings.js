import { Booking } from '../models/bookings.js'

import HttpError from '../models/http-errors.js'

const getAllBookingsForThisUser = async (req, res) => {
  const bookings = await Booking.find({ user: req.params.id }).populate(
    'user',
    'desk'
  )
  res.json(bookings)
}

const getOneBookingByID = async (req, res, next) => {
  let booking
  try {
    booking = await Booking.findById(req.params.id).populate('user', 'desk')
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  res.json(booking)
}

const getAllBookingsByDeskID = async (req, res) => {
  const bookings = await Booking.find({ desk: req.params.id }).populate(
    'user',
    'desk'
  )
  res.json(bookings)
}

const createBooking = async (req, res, next) => {
  const { user, desk, start, end } = req.body

  const booking = new Booking({
    user,
    desk,
    start,
    end,
  })

  const checkBooking = await Booking.find({ desk: desk })

  if (checkBooking.length > 0) {
    checkBooking.forEach((booking) => {
      if (
        (start >= booking.start && start <= booking.end) ||
        (end >= booking.start && end <= booking.end)
      ) {
        return next(new HttpError('Desk not available', 500))
      }
    })
  }

  try {
    await booking.save()
  } catch (error) {
    return next(new HttpError('Cannot create booking', 500))
  }

  res.status(201).json(booking)
}

const findBookingsByDateTimeAndDeskID = async (req, res, next) => {
  const { desk, start, end } = req.body

  const bookings = await Booking.find({
    desk: desk,
    start: { $lte: start },
    end: { $lte: end },
  })

  if (bookings) {
    res.json(bookings)
  } else {
    return next(new HttpError('No bookings found for this time period', 404))
  }
}

const deleteBookingByID = async (req, res, next) => {
  let booking
  try {
    booking = await Booking.findById(req.params.id)
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  try {
    await booking.remove()
  } catch (error) {
    return next(new HttpError('Cannot delete booking', 500))
  }

  res.status(200).json({ message: 'Booking deleted' })
}

export {
  getAllBookingsForThisUser,
  getOneBookingByID,
  createBooking,
  getAllBookingsByDeskID,
  findBookingsByDateTimeAndDeskID,
  deleteBookingByID,
}
