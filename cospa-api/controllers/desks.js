import { Desk } from '../models/desks.js'

import HttpError from '../models/http-errors.js'

const getAllDesksForThisSpace = async (req, res) => {
  const desks = await Desk.find({ space: req.params.id }).populate('space')
  res.json(desks)
}

const getOneDeskByID = async (req, res, next) => {
  let desk
  try {
    desk = await Desk.findById(req.params.id).populate('space')
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  res.json(desk)
}

const getAllDesksForAllSpaces = async (req, res) => {
  const desks = await Desk.find().populate('space')
  res.json(desks)
}

const createDesk = async (req, res, next) => {
  const { name, space, dayprice, hourprice, xcoord, ycoord } = req.body

  const desk = new Desk({
    name,
    space,
    dayprice,
    hourprice,
    xcoord,
    ycoord,
  })

  try {
    await desk.save()
  } catch (error) {
    return next(new HttpError('Cannot create desk', 500))
  }

  res.status(201).json(desk)
}

const deleteDesk = async (req, res, next) => {
  let desk
  try {
    desk = await Desk.findById(req.params.id)
  } catch (error) {
    return next(new HttpError('Cannot find desk', 404))
  }

  try {
    await desk.remove()
  } catch (error) {
    return next(new HttpError('Cannot delete desk', 500))
  }

  res.status(200).json({ message: 'Desk deleted' })
}

export {
  getAllDesksForThisSpace,
  getOneDeskByID,
  getAllDesksForAllSpaces,
  createDesk,
  deleteDesk,
}
