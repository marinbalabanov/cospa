import crypto from 'crypto'
import mongoose from 'mongoose'
import { validationResult, matchedData } from 'express-validator'

import { User, Password } from '../models/users.js'

import HttpError from '../models/http-errors.js'

import { getHash, checkPassword, getToken } from '../common/index.js'

const getAllUsers = async (req, res) => {
  const users = await User.find({}).populate('firstName lastName')
  res.json(users)
}

const getOneUser = async (req, res, next) => {
  let user
  try {
    user = await User.findById(req.params.id).populate('firstName lastName')
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  res.json(user)
}

const signup = async (req, res, next) => {
  const matchData = matchedData(req)

  const unlockKey = crypto.randomUUID()
  const unlockEndsAt = +new Date() + 1000 * 60 * 60 * 24 * 3

  const createdUser = new User({
    ...req.body,
    ...matchData,
    unlockKey,
    unlockEndsAt,
  })

  let newUser
  try {
    const sess = await mongoose.startSession()
    sess.startTransaction()

    newUser = await createdUser.save({ session: sess })

    const createdPassword = new Password({
      user: newUser._id,
      password: getHash(req.body.password),
    })

    await createdPassword.save({ session: sess })

    await sess.commitTransaction()
  } catch (error) {
    return next(new HttpError(error, 422))
  }

  res.send(newUser)
}

const unlock = async (req, res, next) => {
  const { unlock_key: unlockKey } = req.headers
  if (!unlockKey) {
    return next(new HttpError('Expected header is missing', 401))
  }

  let foundUser
  try {
    foundUser = await User.findOne({ unlockKey })

    if (+new Date() > foundUser.unlockEndsAt) {
      return next(new HttpError('User unlock key is invalid', 401))
    }

    foundUser.activated = true
    foundUser.unlockEndsAt = 0
    foundUser.unlockKey = ''
    await foundUser.save()
  } catch {
    return next(new HttpError('Cannot unlock user', 400))
  }

  res.send('User unlocked successfully')
}

const login = async (req, res, next) => {
  const { email, password } = req.body

  let foundUser
  try {
    foundUser = await User.findOne({
      email,
    })

    if (!foundUser.activated) {
      return next(new HttpError('User not yet activated', 401))
    }

    const foundPassword = await Password.findOne({ user: foundUser._id })

    const isPasswordOk = checkPassword(password, foundPassword.password)
    if (!isPasswordOk) {
      return next(new HttpError('Email address or password wrong', 401))
    }

    const token = getToken({ id: foundUser._id })

    res.send(token)
  } catch {
    return next(new HttpError('Cannot login user', 400))
  }
}

const deleteUser = async (req, res, next) => {
  const { id } = req.params

  console.log(req.verifiedUser)
  let user
  try {
    user = await User.findById(id)
    if (!user) {
      return next(new HttpError('Cannot find user', 404))
    }
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  if (!req.verifiedUser.isAdmin) {
    if (req.verifiedUser._id != id) {
      return next(new HttpError('Not permitted to delete user', 403))
    }
  }

  try {
    const session = await mongoose.startSession()

    session.startTransaction()
    await Password.findOneAndDelete({ user: id }, { session })
    await user.deleteOne({ session })
    await session.commitTransaction()
  } catch {
    return next(new HttpError('Error while deleting', 500))
  }
  res.send('User successfully deleted')
}

const changePassword = async (req, res, next) => {
  const result = validationResult(req)

  if (result.errors.length > 0) {
    return next(new HttpError(JSON.stringify(result), 422))
  }

  const matchData = matchedData(req)

  const { oldPassword, newPassword } = matchData

  const foundPassword = await Password.findOne({
    user: req.verifiedUser._id,
  })

  const isPasswordOk = checkPassword(oldPassword, foundPassword.password)
  if (!isPasswordOk) {
    return next(new HttpError('Old password does not match', 401))
  }

  foundPassword.password = getHash(newPassword)
  await foundPassword.save()

  res.send('Password changed successfully')
}

const updateUser = async (req, res, next) => {
  const { id } = req.params
  const result = validationResult(req)

  if (result.errors.length > 0) {
    return next(new HttpError(JSON.stringify(result), 422))
  }

  const matchData = matchedData(req)

  let user
  try {
    user = await User.findById(id)
    if (!user) {
      return next(new HttpError('Cannot find user', 404))
    }
  } catch (error) {
    return next(new HttpError('Cannot find user', 404))
  }

  if (!req.verifiedUser.isAdmin) {
    if (req.verifiedUser._id != id) {
      return next(new HttpError('Not permitted to update user', 403))
    }
  }

  Object.assign(user, matchData)

  const changedUser = await user.save()

  res.json(changedUser)
}

export {
  getAllUsers,
  getOneUser,
  signup,
  unlock,
  login,
  deleteUser,
  changePassword,
  updateUser,
}
