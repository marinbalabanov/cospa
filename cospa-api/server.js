import * as dotenv from 'dotenv'
import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import mongoose from 'mongoose'
import session from 'express-session'
import cookieParser from 'cookie-parser'
import HttpError from './models/http-errors.js'
import router from './routes/router.js'
import swaggerJSDoc from 'swagger-jsdoc'
import swaggerUi from 'swagger-ui-express'

dotenv.config()

const PORT = process.env.PORT || 5000

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'CoSpa API',
    version: '1.0.0',
    description: 'This API allow you to access the CoSpa database',
  },
  servers: [
    {
      url: `http://localhost:${PORT}`,
      description: 'Development server',
    },
  ],
}

const options = {
  swaggerDefinition,
  apis: ['./routes/router.js'],
}

const swaggerSpec = swaggerJSDoc(options)

const app = express()

app.use(
  helmet.contentSecurityPolicy({
    directives: {
      scriptSrc: ["'self'", "'unsafe-inline'", 'https://cdn.jsdelivr.net'],
      styleSrc: ["'self'", "'unsafe-inline'"],
    },
  })
)

// Disable Helmet to make Swagger work locally - Comment out block above and uncomment this block
// Note: This is not recommended for production

// app.use(
//   helmet({
//     contentSecurityPolicy: false,
//   })
// )

app.use(cors())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cookieParser())

app.use(
  session({
    secret: process.env.JWT_SECRET_KEY,
    saveUninitialized: true,
    resave: true,
  })
)

app.get('/', (req, res) => {
  res.send('Server root requested')
})

app.use('/api', router)

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

app.use((req) => {
  throw new HttpError('Cannot find route: ' + req.originalUrl, 404)
})

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error)
  }

  res
    .status(error.errorCode || 500)
    .json({ message: error.message || 'Undefined error' })
})

const CONNECTION_STRING = `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@cluster0.tcmb2xd.mongodb.net/?retryWrites=true&w=majority`

mongoose
  .connect(CONNECTION_STRING)
  .then(() => {
    app.listen(PORT, () => {
      console.log(
        `Express Server running at http://localhost:${PORT}
For API Documentation go to http://localhost:${PORT}/api-docs/`
      )
    })
  })
  .catch((error) => {
    console.log('Cannot connect to MongoDB.', error)
  })
