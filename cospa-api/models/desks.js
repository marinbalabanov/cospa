import mongoose from 'mongoose'

const Schema = mongoose.Schema

const desksSchema = new Schema(
  {
    space: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Space',
    },
    name: { type: String, required: true },
    dayprice: { type: String, required: true },
    hourprice: { type: String, required: true },
    xcoord: { type: String, required: true },
    ycoord: { type: String, required: true },
  },
  { timestamps: true }
)

export const Desk = mongoose.model('Desk', desksSchema)
