import mongoose from 'mongoose'
import { User } from './users.js'
import { Desk } from './desks.js'

const Schema = mongoose.Schema

const bookingsSchema = new Schema(
  {
    user: { type: mongoose.Types.ObjectId, required: true, ref: 'User' },
    desk: { type: mongoose.Types.ObjectId, required: true, ref: 'Desk' },
    start: { type: Date, required: true },
    end: { type: Date, required: true },
  },
  { timestamps: true }
)

export const Booking = mongoose.model('Booking', bookingsSchema)
