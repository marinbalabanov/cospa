import mongoose from 'mongoose'

const Schema = mongoose.Schema

const spacesSchema = new Schema(
  {
    name: { type: String, required: true, unique: true },
    street: { type: String, required: true },
    zip: { type: String, required: true },
    city: { type: String, required: true },
    country: { type: String, required: true },
    phone: { type: String, required: false },
    email: { type: String, required: true },
    website: { type: String, required: false },
    planurl: { type: String, required: false },
    imageurl: { type: String, required: false },
    lat: { type: String, required: false }, // String oder Number???
    lng: { type: String, required: false },
  },
  { timestamps: true }
)

export const Space = mongoose.model('Space', spacesSchema)
