import mongoose from 'mongoose'

const Schema = mongoose.Schema

const usersSchema = new Schema(
  {
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    isAdmin: { type: Boolean, default: false },
    unlockKey: String,
    unlockEndsAt: Number,
    activated: { type: Boolean, default: true },
  },
  { timestamps: true }
)

const passwordsSchema = new Schema(
  {
    password: { type: String, required: true },
    user: { type: mongoose.Types.ObjectId, required: true, ref: 'User' },
  },
  { timestamps: true }
)

export const User = mongoose.model('User', usersSchema)
export const Password = mongoose.model('Password', passwordsSchema)
