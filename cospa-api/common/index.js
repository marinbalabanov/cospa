import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import * as dotenv from "dotenv";

import HttpError from "../models/http-errors.js";

import { User } from "../models/users.js";

dotenv.config();

const SALT_ROUNDS = 10;

const getHash = (plainText) => {
  const hash = bcrypt.hashSync(plainText, SALT_ROUNDS);
  return hash;
};

const checkPassword = (password, hash) => bcrypt.compareSync(password, hash);

const userExists = (users, email) => {
  if (users.findIndex((user) => user.email === email) > -1) {
    return true;
  }

  return false;
};

const getToken = (payload, expiresIn = "1h") =>
  jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn });

const checkToken = async (req, res, next) => {
  if (req.method === "OPTIONS") {
    return next();
  }

  const { authorization } = req.headers;

  if (!authorization) {
    return next(new HttpError("Invalid token", 401));
  }

  const token = authorization.split(" ")[1];

  let decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
  } catch {
    return next(new HttpError("Invalid token", 401));
  }

  let verifiedUser;
  try {
    verifiedUser = await User.findById(decoded.id);
  } catch {
    return next(new HttpError("Invalid token", 401));
  }

  req.verifiedUser = verifiedUser;

  next();
};

export { getHash, userExists, checkPassword, getToken, checkToken };
